import re
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
import cv2 as cv
import numpy as np

img = cv.imread('alphabet.png')
gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
# Now we split the image to 5000 cells, each 20x20 size
cells = [np.hsplit(row,50) for row in np.vsplit(gray,7)]
# Make it into a Numpy array: its size will be (7,50,20,20)
x = np.array(cells)
# Now we prepare the training data and test data
train = x[:,:25].reshape(-1,400).astype(np.float32) # Size = (2500,400)
test = x[:,25:50].reshape(-1,400).astype(np.float32) # Size = (2500,400)
# Create labels for train and test data
k = np.arange(7)
train_labels = np.repeat(k,25)[:,np.newaxis]
test_labels = train_labels.copy()

KNN = KNeighborsClassifier(5)
KNN.fit(train,train_labels)
print(test)
result = KNN.predict(test)
result = result[:,np.newaxis]

matches = result==test_labels
correct = np.count_nonzero(matches)
accuracy = correct*100.0/result.size