import numpy as np
import cv2 as cv

img = cv.imread('alphabet.png')
gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
# Now we split the image to 5000 cells, each 20x20 size
cells = [np.hsplit(row,50) for row in np.vsplit(gray,7)]
# Make it into a Numpy array: its size will be (7,50,20,20)
x = np.array(cells)
# Now we prepare the training data and test data
train = x[:,:25].reshape(-1,400).astype(np.float32) # Size = (2500,400)
test = x[:,25:50].reshape(-1,400).astype(np.float32) # Size = (2500,400)
# Create labels for train and test data
k = np.arange(7)
train_labels = np.repeat(k,25)[:,np.newaxis]
test_labels = train_labels.copy()
# Initiate kNN, train it on the training data, then test it with the test data with k=1
knn = cv.ml.KNearest_create()
knn.train(train, cv.ml.ROW_SAMPLE, train_labels)
print(test)
ret,result,neighbours,dist = knn.findNearest(test,k=5)
# Now we check the accuracy of classification
# For that, compare the result with test_labels and check which are wrong
matches = result==test_labels
print(test_labels)
print(result)
correct = np.count_nonzero(matches)
print(correct)
print(result.size)
accuracy = correct*100.0/result.size
print( accuracy )